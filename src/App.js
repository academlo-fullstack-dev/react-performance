import './App.css';
import TodoContainer from "./components/TodoContainer";

const createArray = (length) => {
  let i = length;
  const arr = [];
  while(i--){
    arr[i] = 0;
  }
  return arr;
};



function App() {
  const myList = createArray(10000);
  return (
    <div className="App">
      <TodoContainer arreglo={myList} />
    </div>
  );
}

export default App;
