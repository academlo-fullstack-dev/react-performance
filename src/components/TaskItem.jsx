import React from 'react';

const TaskItem = React.memo((props) => {
    console.log("TaskItem renderizando");
    return (
        <>
           <h2>{props.item.title}</h2>
           <button onClick={() => props.removeTaskFn(props.item.id)}>Eliminar tarea</button>
        </>
    )
});

export default TaskItem;

// () => {} === () => {} false
