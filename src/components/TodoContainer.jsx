import React, { useCallback, useState, useMemo } from "react";
import TaskList from "./TaskList";

// React.Memo para memorizar componentes
// useCallback para memorizar funciones
// useMemo para memorizar valores
const todoArray = [
  {
    id: 1,
    title: "Ir al gimnasio",
  },
  {
    id: 2,
    title: "Ir por las compras de la semana",
  },
  {
    id: 3,
    title: "Lavar la ropa",
  },
  {
    id: 4,
    title: "Hacer la quinta actividad del modulo frontend",
  },
];

const procesoPesado = (array) => {
  console.log("Realizando los calculos");
  let c = 0;
  for(let i = 0; i < array.length; i++){
    for(let j=0; j < array.length; j++){
      c = c + 1;
    }
  }
  return c;
}

const TodoContainer = (props) => {

  const result = useMemo(() => procesoPesado(props.arreglo), [props.arreglo]);

  console.log("Todo Container renderizado");
  const [todoList, setTodoList] = useState(todoArray);

  const [newTodo, setNewTodo] = useState({ title: "" });

  const removeTask = useCallback(
    (id) => {
      setTodoList(todoList.filter((task) => task.id !== id));
    },
    [todoList]
  );

  const handleChange = (event) => {
    const newTodoObj = {
      id: 7,
      title: event.target.value,
    };
    setNewTodo(newTodoObj);
  };

  return (
    <>
      <h6>Iteraciones: {result}</h6>
      <h4>Agregar una nueva tarea</h4>
      <input
        onChange={handleChange}
        value={newTodo.title}
        placeholder="nueva tarea"
      />
      <h2>Lista de tareas</h2>
      <TaskList todoList={todoList} removeTaskFn={removeTask} />
    </>
  );
};

export default TodoContainer;
