import React from "react";
import TaskItem from "./TaskItem";

const TaskList = React.memo((props) => {
    console.log("TaskList renderizado");
    return (
        <>
           {
               props.todoList.map(task => <TaskItem removeTaskFn={props.removeTaskFn} key={task.id} item={task} />)
           }
        </>
    )
});

export default TaskList;
